import 'dart:async';

import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/app/app_bloc.dart';
import 'package:cat_app/domain/blocs/app/app_state.dart';
import 'package:cat_app/presentation/login/login_page.dart';
import 'package:cat_app/presentation/main/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runZoned<Future<void>>(
        () async {
          await configureDependencies();
          runApp(CatApplication());
    },
  );
}

class CatApplication extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cat Application',
      theme: ThemeData(
        primarySwatch: Colors.teal,
        accentColor: Colors.teal.shade500,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocBuilder(
        cubit: getIt.get<AppBloc>(),
        builder: (context, state) {
          return _renderStateAndBuildWidget(state);
        },
      )
    );
  }

  Widget _renderStateAndBuildWidget(AppState state) {
    switch(state.runtimeType) {
      case UserUnauthorizedState:
        return LoginPage();
        break;
      case UserLoggedInState:
        return MainPage();
        break;
      case InitialState:
        return Center(child: CircularProgressIndicator());
        break;
    }
  }
}

