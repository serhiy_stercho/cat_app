// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favourite_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavouriteResponse _$FavouriteResponseFromJson(Map<String, dynamic> json) {
  return FavouriteResponse(
    json['image'] == null
        ? null
        : CatImage.fromJson(json['image'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$FavouriteResponseToJson(FavouriteResponse instance) =>
    <String, dynamic>{
      'image': instance.image,
    };
