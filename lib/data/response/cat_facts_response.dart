
import 'package:cat_app/data/model/cat/cat_fact.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cat_facts_response.g.dart';

@JsonSerializable()
class CatFactsResponse {

    @JsonKey(name: 'current_page')
    int currentPage;
    @JsonKey(name: 'last_page')
    int lastPage;
    @JsonKey(name: 'data')
    List<CatFact> catFacts;

    CatFactsResponse(this.currentPage, this.lastPage, this.catFacts);

    factory CatFactsResponse.fromJson(Map<String, dynamic> json) => _$CatFactsResponseFromJson(json);
    Map<String, dynamic> toJson() => _$CatFactsResponseToJson(this);
}