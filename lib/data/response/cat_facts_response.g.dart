// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_facts_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatFactsResponse _$CatFactsResponseFromJson(Map<String, dynamic> json) {
  return CatFactsResponse(
    json['current_page'] as int,
    json['last_page'] as int,
    (json['data'] as List)
        ?.map((e) =>
            e == null ? null : CatFact.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CatFactsResponseToJson(CatFactsResponse instance) =>
    <String, dynamic>{
      'current_page': instance.currentPage,
      'last_page': instance.lastPage,
      'data': instance.catFacts,
    };
