
import 'package:cat_app/data/model/cat/cat_image.dart';
import 'package:json_annotation/json_annotation.dart';

part 'favourite_response.g.dart';

@JsonSerializable()
class FavouriteResponse {

  CatImage image;

  FavouriteResponse(this.image);

  factory FavouriteResponse.fromJson(Map<String, dynamic> json) => _$FavouriteResponseFromJson(json);
  Map<String, dynamic> toJson() => _$FavouriteResponseToJson(this);
}