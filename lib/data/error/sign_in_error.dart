

class SignInError implements Exception {

  String errorMessage;

  SignInError(this.errorMessage);

  @override
  String toString() {
    return errorMessage;
  }
}