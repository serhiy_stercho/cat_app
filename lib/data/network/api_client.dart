

import 'dart:convert';

import 'package:cat_app/common/consants.dart';
import 'package:cat_app/data/model/cat/cat_fact.dart';
import 'package:cat_app/data/model/cat/cat_image.dart';
import 'package:cat_app/data/response/cat_facts_response.dart';
import 'package:cat_app/data/response/favourite_response.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@singleton
class ApiClient {

  Dio _factsApiDioClient;
  Dio _imagesApiDioClient;

  ApiClient(@Named('images_dio_client') this._imagesApiDioClient, @Named('facts_dio_client') this._factsApiDioClient);


  Future<List<CatImage>> getCatImages(int page) async {
      final response = await _imagesApiDioClient.get('images/search', queryParameters: {'limit': Constants.per_page, 'page': page});
      final responseData = response.data as List;
      return responseData.map<CatImage>((e) => CatImage.fromJson(e)).toList();
  }

  Future<List<CatFact>> getCatFacts(int page) async {
    final response = await _factsApiDioClient.get('facts', queryParameters: {'limit': Constants.per_page, 'page': page});
    final responseData = CatFactsResponse.fromJson(response.data);
    return responseData.catFacts;
  }

  Future<List<CatImage>> getFavouritesList(int page, String userId) async {
    final response = await _imagesApiDioClient.get('favourites', queryParameters: {'limit': Constants.per_page, 'page': page, "sub_id": userId});
    final responseData = response.data as List;
    return responseData
        .map<CatImage>((e) => FavouriteResponse.fromJson(e).image).toList();
  }

  Future<String> saveAsFavourite(String imageId, String userId) async {
    final response = await _imagesApiDioClient.post('favourites', data: {"image_id": imageId, "sub_id": userId});
    if(response.statusCode == 200)
      return imageId;

    return null;
  }

  Future<String> deleteFromFavourites(String imageId) async {
    final response = await _imagesApiDioClient.delete('favourites/{$imageId}');
    if(response.statusCode == 200)
      return imageId;

    return null;
  }
}