

import 'package:moor/moor.dart';


class CatDataEntity extends Table {
  TextColumn get id => text()();
  TextColumn get fact => text()();
  TextColumn get imageUrl => text().named('image_url')();
  BoolColumn get isFavorite => boolean().named('favorite')();

  TextColumn get base64Image => text().nullable()();
}