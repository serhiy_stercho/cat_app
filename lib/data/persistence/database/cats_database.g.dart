// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cats_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class CatDataEntityData extends DataClass
    implements Insertable<CatDataEntityData> {
  final String id;
  final String fact;
  final String imageUrl;
  final bool isFavorite;
  final String base64Image;
  CatDataEntityData(
      {@required this.id,
      @required this.fact,
      @required this.imageUrl,
      @required this.isFavorite,
      this.base64Image});
  factory CatDataEntityData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return CatDataEntityData(
      id: stringType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      fact: stringType.mapFromDatabaseResponse(data['${effectivePrefix}fact']),
      imageUrl: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_url']),
      isFavorite:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}favorite']),
      base64Image: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}base64_image']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<String>(id);
    }
    if (!nullToAbsent || fact != null) {
      map['fact'] = Variable<String>(fact);
    }
    if (!nullToAbsent || imageUrl != null) {
      map['image_url'] = Variable<String>(imageUrl);
    }
    if (!nullToAbsent || isFavorite != null) {
      map['favorite'] = Variable<bool>(isFavorite);
    }
    if (!nullToAbsent || base64Image != null) {
      map['base64_image'] = Variable<String>(base64Image);
    }
    return map;
  }

  CatDataEntityCompanion toCompanion(bool nullToAbsent) {
    return CatDataEntityCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      fact: fact == null && nullToAbsent ? const Value.absent() : Value(fact),
      imageUrl: imageUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(imageUrl),
      isFavorite: isFavorite == null && nullToAbsent
          ? const Value.absent()
          : Value(isFavorite),
      base64Image: base64Image == null && nullToAbsent
          ? const Value.absent()
          : Value(base64Image),
    );
  }

  factory CatDataEntityData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return CatDataEntityData(
      id: serializer.fromJson<String>(json['id']),
      fact: serializer.fromJson<String>(json['fact']),
      imageUrl: serializer.fromJson<String>(json['imageUrl']),
      isFavorite: serializer.fromJson<bool>(json['isFavorite']),
      base64Image: serializer.fromJson<String>(json['base64Image']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'fact': serializer.toJson<String>(fact),
      'imageUrl': serializer.toJson<String>(imageUrl),
      'isFavorite': serializer.toJson<bool>(isFavorite),
      'base64Image': serializer.toJson<String>(base64Image),
    };
  }

  CatDataEntityData copyWith(
          {String id,
          String fact,
          String imageUrl,
          bool isFavorite,
          String base64Image}) =>
      CatDataEntityData(
        id: id ?? this.id,
        fact: fact ?? this.fact,
        imageUrl: imageUrl ?? this.imageUrl,
        isFavorite: isFavorite ?? this.isFavorite,
        base64Image: base64Image ?? this.base64Image,
      );
  @override
  String toString() {
    return (StringBuffer('CatDataEntityData(')
          ..write('id: $id, ')
          ..write('fact: $fact, ')
          ..write('imageUrl: $imageUrl, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('base64Image: $base64Image')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          fact.hashCode,
          $mrjc(imageUrl.hashCode,
              $mrjc(isFavorite.hashCode, base64Image.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is CatDataEntityData &&
          other.id == this.id &&
          other.fact == this.fact &&
          other.imageUrl == this.imageUrl &&
          other.isFavorite == this.isFavorite &&
          other.base64Image == this.base64Image);
}

class CatDataEntityCompanion extends UpdateCompanion<CatDataEntityData> {
  final Value<String> id;
  final Value<String> fact;
  final Value<String> imageUrl;
  final Value<bool> isFavorite;
  final Value<String> base64Image;
  const CatDataEntityCompanion({
    this.id = const Value.absent(),
    this.fact = const Value.absent(),
    this.imageUrl = const Value.absent(),
    this.isFavorite = const Value.absent(),
    this.base64Image = const Value.absent(),
  });
  CatDataEntityCompanion.insert({
    @required String id,
    @required String fact,
    @required String imageUrl,
    @required bool isFavorite,
    this.base64Image = const Value.absent(),
  })  : id = Value(id),
        fact = Value(fact),
        imageUrl = Value(imageUrl),
        isFavorite = Value(isFavorite);
  static Insertable<CatDataEntityData> custom({
    Expression<String> id,
    Expression<String> fact,
    Expression<String> imageUrl,
    Expression<bool> isFavorite,
    Expression<String> base64Image,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (fact != null) 'fact': fact,
      if (imageUrl != null) 'image_url': imageUrl,
      if (isFavorite != null) 'favorite': isFavorite,
      if (base64Image != null) 'base64_image': base64Image,
    });
  }

  CatDataEntityCompanion copyWith(
      {Value<String> id,
      Value<String> fact,
      Value<String> imageUrl,
      Value<bool> isFavorite,
      Value<String> base64Image}) {
    return CatDataEntityCompanion(
      id: id ?? this.id,
      fact: fact ?? this.fact,
      imageUrl: imageUrl ?? this.imageUrl,
      isFavorite: isFavorite ?? this.isFavorite,
      base64Image: base64Image ?? this.base64Image,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (fact.present) {
      map['fact'] = Variable<String>(fact.value);
    }
    if (imageUrl.present) {
      map['image_url'] = Variable<String>(imageUrl.value);
    }
    if (isFavorite.present) {
      map['favorite'] = Variable<bool>(isFavorite.value);
    }
    if (base64Image.present) {
      map['base64_image'] = Variable<String>(base64Image.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('CatDataEntityCompanion(')
          ..write('id: $id, ')
          ..write('fact: $fact, ')
          ..write('imageUrl: $imageUrl, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('base64Image: $base64Image')
          ..write(')'))
        .toString();
  }
}

class $CatDataEntityTable extends CatDataEntity
    with TableInfo<$CatDataEntityTable, CatDataEntityData> {
  final GeneratedDatabase _db;
  final String _alias;
  $CatDataEntityTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedTextColumn _id;
  @override
  GeneratedTextColumn get id => _id ??= _constructId();
  GeneratedTextColumn _constructId() {
    return GeneratedTextColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _factMeta = const VerificationMeta('fact');
  GeneratedTextColumn _fact;
  @override
  GeneratedTextColumn get fact => _fact ??= _constructFact();
  GeneratedTextColumn _constructFact() {
    return GeneratedTextColumn(
      'fact',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageUrlMeta = const VerificationMeta('imageUrl');
  GeneratedTextColumn _imageUrl;
  @override
  GeneratedTextColumn get imageUrl => _imageUrl ??= _constructImageUrl();
  GeneratedTextColumn _constructImageUrl() {
    return GeneratedTextColumn(
      'image_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _isFavoriteMeta = const VerificationMeta('isFavorite');
  GeneratedBoolColumn _isFavorite;
  @override
  GeneratedBoolColumn get isFavorite => _isFavorite ??= _constructIsFavorite();
  GeneratedBoolColumn _constructIsFavorite() {
    return GeneratedBoolColumn(
      'favorite',
      $tableName,
      false,
    );
  }

  final VerificationMeta _base64ImageMeta =
      const VerificationMeta('base64Image');
  GeneratedTextColumn _base64Image;
  @override
  GeneratedTextColumn get base64Image =>
      _base64Image ??= _constructBase64Image();
  GeneratedTextColumn _constructBase64Image() {
    return GeneratedTextColumn(
      'base64_image',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, fact, imageUrl, isFavorite, base64Image];
  @override
  $CatDataEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'cat_data_entity';
  @override
  final String actualTableName = 'cat_data_entity';
  @override
  VerificationContext validateIntegrity(Insertable<CatDataEntityData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('fact')) {
      context.handle(
          _factMeta, fact.isAcceptableOrUnknown(data['fact'], _factMeta));
    } else if (isInserting) {
      context.missing(_factMeta);
    }
    if (data.containsKey('image_url')) {
      context.handle(_imageUrlMeta,
          imageUrl.isAcceptableOrUnknown(data['image_url'], _imageUrlMeta));
    } else if (isInserting) {
      context.missing(_imageUrlMeta);
    }
    if (data.containsKey('favorite')) {
      context.handle(_isFavoriteMeta,
          isFavorite.isAcceptableOrUnknown(data['favorite'], _isFavoriteMeta));
    } else if (isInserting) {
      context.missing(_isFavoriteMeta);
    }
    if (data.containsKey('base64_image')) {
      context.handle(
          _base64ImageMeta,
          base64Image.isAcceptableOrUnknown(
              data['base64_image'], _base64ImageMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  CatDataEntityData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return CatDataEntityData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $CatDataEntityTable createAlias(String alias) {
    return $CatDataEntityTable(_db, alias);
  }
}

class FavoriteDataEntityData extends DataClass
    implements Insertable<FavoriteDataEntityData> {
  final String id;
  final String fact;
  final String imageUrl;
  final bool isFavorite;
  final String base64Image;
  FavoriteDataEntityData(
      {@required this.id,
      @required this.fact,
      @required this.imageUrl,
      @required this.isFavorite,
      this.base64Image});
  factory FavoriteDataEntityData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return FavoriteDataEntityData(
      id: stringType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      fact: stringType.mapFromDatabaseResponse(data['${effectivePrefix}fact']),
      imageUrl: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_url']),
      isFavorite:
          boolType.mapFromDatabaseResponse(data['${effectivePrefix}favorite']),
      base64Image: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}base64_image']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<String>(id);
    }
    if (!nullToAbsent || fact != null) {
      map['fact'] = Variable<String>(fact);
    }
    if (!nullToAbsent || imageUrl != null) {
      map['image_url'] = Variable<String>(imageUrl);
    }
    if (!nullToAbsent || isFavorite != null) {
      map['favorite'] = Variable<bool>(isFavorite);
    }
    if (!nullToAbsent || base64Image != null) {
      map['base64_image'] = Variable<String>(base64Image);
    }
    return map;
  }

  FavoriteDataEntityCompanion toCompanion(bool nullToAbsent) {
    return FavoriteDataEntityCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      fact: fact == null && nullToAbsent ? const Value.absent() : Value(fact),
      imageUrl: imageUrl == null && nullToAbsent
          ? const Value.absent()
          : Value(imageUrl),
      isFavorite: isFavorite == null && nullToAbsent
          ? const Value.absent()
          : Value(isFavorite),
      base64Image: base64Image == null && nullToAbsent
          ? const Value.absent()
          : Value(base64Image),
    );
  }

  factory FavoriteDataEntityData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return FavoriteDataEntityData(
      id: serializer.fromJson<String>(json['id']),
      fact: serializer.fromJson<String>(json['fact']),
      imageUrl: serializer.fromJson<String>(json['imageUrl']),
      isFavorite: serializer.fromJson<bool>(json['isFavorite']),
      base64Image: serializer.fromJson<String>(json['base64Image']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<String>(id),
      'fact': serializer.toJson<String>(fact),
      'imageUrl': serializer.toJson<String>(imageUrl),
      'isFavorite': serializer.toJson<bool>(isFavorite),
      'base64Image': serializer.toJson<String>(base64Image),
    };
  }

  FavoriteDataEntityData copyWith(
          {String id,
          String fact,
          String imageUrl,
          bool isFavorite,
          String base64Image}) =>
      FavoriteDataEntityData(
        id: id ?? this.id,
        fact: fact ?? this.fact,
        imageUrl: imageUrl ?? this.imageUrl,
        isFavorite: isFavorite ?? this.isFavorite,
        base64Image: base64Image ?? this.base64Image,
      );
  @override
  String toString() {
    return (StringBuffer('FavoriteDataEntityData(')
          ..write('id: $id, ')
          ..write('fact: $fact, ')
          ..write('imageUrl: $imageUrl, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('base64Image: $base64Image')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          fact.hashCode,
          $mrjc(imageUrl.hashCode,
              $mrjc(isFavorite.hashCode, base64Image.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is FavoriteDataEntityData &&
          other.id == this.id &&
          other.fact == this.fact &&
          other.imageUrl == this.imageUrl &&
          other.isFavorite == this.isFavorite &&
          other.base64Image == this.base64Image);
}

class FavoriteDataEntityCompanion
    extends UpdateCompanion<FavoriteDataEntityData> {
  final Value<String> id;
  final Value<String> fact;
  final Value<String> imageUrl;
  final Value<bool> isFavorite;
  final Value<String> base64Image;
  const FavoriteDataEntityCompanion({
    this.id = const Value.absent(),
    this.fact = const Value.absent(),
    this.imageUrl = const Value.absent(),
    this.isFavorite = const Value.absent(),
    this.base64Image = const Value.absent(),
  });
  FavoriteDataEntityCompanion.insert({
    @required String id,
    @required String fact,
    @required String imageUrl,
    @required bool isFavorite,
    this.base64Image = const Value.absent(),
  })  : id = Value(id),
        fact = Value(fact),
        imageUrl = Value(imageUrl),
        isFavorite = Value(isFavorite);
  static Insertable<FavoriteDataEntityData> custom({
    Expression<String> id,
    Expression<String> fact,
    Expression<String> imageUrl,
    Expression<bool> isFavorite,
    Expression<String> base64Image,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (fact != null) 'fact': fact,
      if (imageUrl != null) 'image_url': imageUrl,
      if (isFavorite != null) 'favorite': isFavorite,
      if (base64Image != null) 'base64_image': base64Image,
    });
  }

  FavoriteDataEntityCompanion copyWith(
      {Value<String> id,
      Value<String> fact,
      Value<String> imageUrl,
      Value<bool> isFavorite,
      Value<String> base64Image}) {
    return FavoriteDataEntityCompanion(
      id: id ?? this.id,
      fact: fact ?? this.fact,
      imageUrl: imageUrl ?? this.imageUrl,
      isFavorite: isFavorite ?? this.isFavorite,
      base64Image: base64Image ?? this.base64Image,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<String>(id.value);
    }
    if (fact.present) {
      map['fact'] = Variable<String>(fact.value);
    }
    if (imageUrl.present) {
      map['image_url'] = Variable<String>(imageUrl.value);
    }
    if (isFavorite.present) {
      map['favorite'] = Variable<bool>(isFavorite.value);
    }
    if (base64Image.present) {
      map['base64_image'] = Variable<String>(base64Image.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoriteDataEntityCompanion(')
          ..write('id: $id, ')
          ..write('fact: $fact, ')
          ..write('imageUrl: $imageUrl, ')
          ..write('isFavorite: $isFavorite, ')
          ..write('base64Image: $base64Image')
          ..write(')'))
        .toString();
  }
}

class $FavoriteDataEntityTable extends FavoriteDataEntity
    with TableInfo<$FavoriteDataEntityTable, FavoriteDataEntityData> {
  final GeneratedDatabase _db;
  final String _alias;
  $FavoriteDataEntityTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedTextColumn _id;
  @override
  GeneratedTextColumn get id => _id ??= _constructId();
  GeneratedTextColumn _constructId() {
    return GeneratedTextColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _factMeta = const VerificationMeta('fact');
  GeneratedTextColumn _fact;
  @override
  GeneratedTextColumn get fact => _fact ??= _constructFact();
  GeneratedTextColumn _constructFact() {
    return GeneratedTextColumn(
      'fact',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageUrlMeta = const VerificationMeta('imageUrl');
  GeneratedTextColumn _imageUrl;
  @override
  GeneratedTextColumn get imageUrl => _imageUrl ??= _constructImageUrl();
  GeneratedTextColumn _constructImageUrl() {
    return GeneratedTextColumn(
      'image_url',
      $tableName,
      false,
    );
  }

  final VerificationMeta _isFavoriteMeta = const VerificationMeta('isFavorite');
  GeneratedBoolColumn _isFavorite;
  @override
  GeneratedBoolColumn get isFavorite => _isFavorite ??= _constructIsFavorite();
  GeneratedBoolColumn _constructIsFavorite() {
    return GeneratedBoolColumn(
      'favorite',
      $tableName,
      false,
    );
  }

  final VerificationMeta _base64ImageMeta =
      const VerificationMeta('base64Image');
  GeneratedTextColumn _base64Image;
  @override
  GeneratedTextColumn get base64Image =>
      _base64Image ??= _constructBase64Image();
  GeneratedTextColumn _constructBase64Image() {
    return GeneratedTextColumn(
      'base64_image',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, fact, imageUrl, isFavorite, base64Image];
  @override
  $FavoriteDataEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'favorite_data_entity';
  @override
  final String actualTableName = 'favorite_data_entity';
  @override
  VerificationContext validateIntegrity(
      Insertable<FavoriteDataEntityData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    } else if (isInserting) {
      context.missing(_idMeta);
    }
    if (data.containsKey('fact')) {
      context.handle(
          _factMeta, fact.isAcceptableOrUnknown(data['fact'], _factMeta));
    } else if (isInserting) {
      context.missing(_factMeta);
    }
    if (data.containsKey('image_url')) {
      context.handle(_imageUrlMeta,
          imageUrl.isAcceptableOrUnknown(data['image_url'], _imageUrlMeta));
    } else if (isInserting) {
      context.missing(_imageUrlMeta);
    }
    if (data.containsKey('favorite')) {
      context.handle(_isFavoriteMeta,
          isFavorite.isAcceptableOrUnknown(data['favorite'], _isFavoriteMeta));
    } else if (isInserting) {
      context.missing(_isFavoriteMeta);
    }
    if (data.containsKey('base64_image')) {
      context.handle(
          _base64ImageMeta,
          base64Image.isAcceptableOrUnknown(
              data['base64_image'], _base64ImageMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  FavoriteDataEntityData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return FavoriteDataEntityData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $FavoriteDataEntityTable createAlias(String alias) {
    return $FavoriteDataEntityTable(_db, alias);
  }
}

abstract class _$CatsDatabase extends GeneratedDatabase {
  _$CatsDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $CatDataEntityTable _catDataEntity;
  $CatDataEntityTable get catDataEntity =>
      _catDataEntity ??= $CatDataEntityTable(this);
  $FavoriteDataEntityTable _favoriteDataEntity;
  $FavoriteDataEntityTable get favoriteDataEntity =>
      _favoriteDataEntity ??= $FavoriteDataEntityTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [catDataEntity, favoriteDataEntity];
}
