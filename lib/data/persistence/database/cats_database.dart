
import 'dart:io';

import 'package:cat_app/data/persistence/entities/cat_data_entity.dart';
import 'package:cat_app/data/persistence/entities/favorite_data_entity.dart';
import 'package:injectable/injectable.dart';
import 'package:moor/ffi.dart';
import 'package:moor/moor.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;

part 'cats_database.g.dart';

@singleton
@UseMoor(tables: [CatDataEntity, FavoriteDataEntity])
class CatsDatabase extends _$CatsDatabase {

  CatsDatabase() : super(_obtainDbConnection());

  @override
  int get schemaVersion => 2;

}

LazyDatabase _obtainDbConnection()  {
  return LazyDatabase(() async {
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(path.join(dbFolder.path, 'cats_database.sqlite'));
    return VmDatabase(file);
  });
}
