
import 'dart:convert';

import 'package:cat_app/common/consants.dart';
import 'package:cat_app/common/enums/auth_provider.dart';
import 'package:cat_app/data/model/user/user_model.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@singleton
class UserRepository {
  
  SharedPreferences _sharedPreferences;
  
  UserRepository(this._sharedPreferences);
  
  bool checkUserLoggedIn() {
    return _getUserJsonString() != null;
  }

  UserModel getCurrentUser()  {
    String userJsonString = _getUserJsonString();
    return UserModel.fromJson(jsonDecode(userJsonString));
  }

  void clearUserData() {
    _sharedPreferences.clear();
  }

  String _getUserJsonString() {
    return _sharedPreferences.getString(Constants.user_model_pref_key);
  }

  String getAuthProvider() {
    return _sharedPreferences.getString(Constants.auth_provider_pref_key);
  }
}