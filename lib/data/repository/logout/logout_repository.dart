
import 'package:cat_app/common/enums/auth_provider.dart';
import 'package:cat_app/data/repository/user/user_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

@singleton
class LogoutRepository {

  UserRepository _userRepository;
  GoogleSignIn _googleSignIn;
  FacebookLogin _facebookLogin;

  LogoutRepository(this._userRepository, this._googleSignIn, this._facebookLogin);

  Future<void> logout() async {
    if(describeEnum(AuthProvider.google)
        == _userRepository.getAuthProvider()) {
      if(await _googleSignIn.isSignedIn())
        await _googleSignIn.signOut();
    } else {
      if(await _facebookLogin.isLoggedIn) {
        await _facebookLogin.logOut();
      }
    }
    _userRepository.clearUserData();
  }


}