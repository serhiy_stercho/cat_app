import 'dart:convert';

import 'package:cat_app/common/consants.dart';
import 'package:cat_app/common/enums/auth_provider.dart';
import 'package:cat_app/data/error/sign_in_error.dart';
import 'package:cat_app/data/model/user/user_model.dart';
import 'package:cat_app/data/repository/sign_in/sign_in_reposirory.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@named
@Singleton(as: SignInRepository)
class FacebookSignInRepository
    implements SignInRepository {

  FacebookLogin _facebookLoginObj;
  SharedPreferences _sharedPreferences;

  FacebookSignInRepository(this._facebookLoginObj, this._sharedPreferences);

  @override
  Future<void> performSignIn() async {
    try {
      final result = await _facebookLoginObj.logIn(['email']);
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final fbAccessToken = result.accessToken.token;
          final profileResponse = await Dio()
              .get('https://graph.facebook.com/v2.12/me',
              queryParameters: {
                'fields': 'name,email,picture',
                'access_token': fbAccessToken
              });
          final profile = json.decode(profileResponse.data);
          final userModel = UserModel(profile['id'], profile['email'], profile['name'], avatarUrl: profile['picture']['data']['url']);
          await _storeUserToPrefs(userModel);
          await _storeAuthProvider();
          break;
        case FacebookLoginStatus.error:
          throw SignInError(result.errorMessage);
          break;
        case FacebookLoginStatus.cancelledByUser:
          break;
      }
    } catch (exception) {
      throw SignInError(exception.toString());
    }
  }

  @override
  Future<void> _storeUserToPrefs(UserModel user) async {
    await _sharedPreferences.setString(Constants.user_model_pref_key, jsonEncode(user));
  }

  @override
  Future<void> _storeAuthProvider() async {
    await _sharedPreferences.setString(Constants.auth_provider_pref_key, describeEnum(AuthProvider.facebook));
  }

}