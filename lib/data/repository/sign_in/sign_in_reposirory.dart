

import 'package:cat_app/data/model/user/user_model.dart';

abstract class SignInRepository {

  Future<void> performSignIn();
  Future<void> _storeUserToPrefs(UserModel user);
  Future<void> _storeAuthProvider();
}