

import 'dart:convert';

import 'package:cat_app/common/consants.dart';
import 'package:cat_app/common/enums/auth_provider.dart';
import 'package:cat_app/data/error/sign_in_error.dart';
import 'package:cat_app/data/model/user/user_model.dart';
import 'package:cat_app/data/repository/sign_in/sign_in_reposirory.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@named
@Singleton(as: SignInRepository)
class GoogleSignInRepository implements SignInRepository {

  GoogleSignIn _googleSignInObj;
  SharedPreferences _sharedPreferences;

  GoogleSignInRepository(this._googleSignInObj, this._sharedPreferences);

  @override
  Future<void> performSignIn() async {
    try {
      GoogleSignInAccount account = await _googleSignInObj.signIn();
      GoogleSignInAuthentication auth = await account.authentication;
      UserModel userModel = UserModel(account.id, account.email, account.displayName, avatarUrl: account.photoUrl);
      await _storeUserToPrefs(userModel);
      await _storeAuthProvider();
    } catch(exception) {
      throw SignInError(exception.toString());
    }
  }

  @override
  Future<void> _storeUserToPrefs(UserModel user) async {
    await _sharedPreferences.setString(Constants.user_model_pref_key, jsonEncode(user));
  }

  @override
  Future<void> _storeAuthProvider() async {
    await _sharedPreferences.setString(Constants.auth_provider_pref_key, describeEnum(AuthProvider.google));
  }
}