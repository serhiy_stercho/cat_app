
import 'package:cat_app/data/mapper/cat_data_to_cat_model_mapper.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/model/cat/cat_fact.dart';
import 'package:cat_app/data/model/cat/cat_image.dart';
import 'package:cat_app/data/network/api_client.dart';
import 'package:cat_app/data/repository/cats_data/local_cats_data_repository.dart';
import 'package:cat_app/data/repository/user/user_repository.dart';
import 'package:injectable/injectable.dart';

@singleton
class CatsDataRepository {

  ApiClient _apiClient;

  CatDataToCatModelMapper catDataToCatModelMapper;
  LocalCatsDataRepository _localCatsDataRepository;
  UserRepository _userRepository;

  CatsDataRepository(this._apiClient, this.catDataToCatModelMapper, this._userRepository, this._localCatsDataRepository);

  Future<List<CatDataModel>> getCatsData(int page) async {
    final catsData = await Future.wait([_apiClient.getCatImages(page), _apiClient.getCatFacts(page)]);
    final catDataModels = List.generate(catsData[0].length, (index) =>
        catDataToCatModelMapper.map(catsData[0][index], catsData[1][index]));
    if(page == 0)
      _localCatsDataRepository.deleteAll();
    await _localCatsDataRepository.insertAll(catDataModels);
    return catDataModels;
  }

  Future<List<CatDataModel>> getFavouritesData(int page) async {
    final currentUserId =  _userRepository.getCurrentUser().id;
    final catsData = await Future.wait([_apiClient.getFavouritesList(page, currentUserId), _apiClient.getCatFacts(page)]);
    final catDataModels = List.generate(catsData[0].length, (index) =>
        catDataToCatModelMapper.map(catsData[0][index], catsData[1][index], favorite: true));
    if(page == 0)
      _localCatsDataRepository.deleteAllFavorites();
    await _localCatsDataRepository.insertAllFavorites(catDataModels);
    return catDataModels;
  }

  Future<String> saveAsFavourite(String imageId) async {
    final currentUserId =  _userRepository.getCurrentUser().id;
    final response = await _apiClient.saveAsFavourite(imageId, currentUserId);
    _localCatsDataRepository.updateFavourite(imageId, true);
    return response;
  }

  Future<String> deleteFromFavourites(String imageId) async {
    final response = await _apiClient.deleteFromFavourites(imageId);
    return response;
  }

}