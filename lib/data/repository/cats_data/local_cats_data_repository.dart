import 'package:cat_app/common/extension/extensions.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/persistence/database/cats_database.dart';
import 'package:injectable/injectable.dart';
import 'package:moor/moor.dart';

@singleton
class LocalCatsDataRepository {
  CatsDatabase database;

  LocalCatsDataRepository(this.database);

  Future<List<CatDataModel>> fetchAll() async {
    final entities = await database.select(database.catDataEntity).get();
    return entities.map((entity) => entity.mapToCatDataModel()).toList();
  }

  Future<void> insertAll(List<CatDataModel> dataModels) async {
    final entities =
        dataModels.map((model) => model.mapToCatDataEntity()).toList();
    await database.batch((batch) {
      batch.insertAll(database.catDataEntity, entities);
    });
  }

  Future<void> insertAllFavorites(List<CatDataModel> dataModels) async {
    final entities =
    dataModels.map((model) => model.mapToFaforiteDataEntity()).toList();
    await database.batch((batch) {
      batch.insertAll(database.favoriteDataEntity, entities);
    });
  }

  Future<List<CatDataModel>> selectAllFavorites() async {
    final entities = await database.select(database.favoriteDataEntity).get();
    return entities.map<CatDataModel>((entity) => entity.mapToCatDataModel()).toList();
  }

  Future deleteAll() {
    return database.delete(database.catDataEntity).go();
  }

  Future deleteAllFavorites() {
    return database.delete(database.favoriteDataEntity).go();
  }

  Future updateFavourite(String id, bool favourite) {
    return (database.update(database.catDataEntity)
          ..where((item) => item.id.equals(id)))
        .write(CatDataEntityCompanion(isFavorite: Value(favourite)));
  }
}
