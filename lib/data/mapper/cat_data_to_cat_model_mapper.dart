
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/model/cat/cat_fact.dart';
import 'package:cat_app/data/model/cat/cat_image.dart';
import 'package:injectable/injectable.dart';

@injectable
class CatDataToCatModelMapper {

  CatDataModel map(CatImage imageData, CatFact factData, {bool favorite = false}) {
    CatDataModel catModel = CatDataModel();
    catModel.id = imageData.id;
    catModel.fact = factData.fact;
    catModel.imageUrl = imageData.url;
    catModel.isFavorite = favorite;

    return catModel;
  }
}