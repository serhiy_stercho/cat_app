

import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {

  String id;
  String email;
  String displayName;
  String avatarUrl;

  UserModel(
      this.id,
      this.email,
      this.displayName,
      {this.avatarUrl});

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);

}