// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_fact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CatFact _$CatFactFromJson(Map<String, dynamic> json) {
  return CatFact(
    json['fact'] as String,
  );
}

Map<String, dynamic> _$CatFactToJson(CatFact instance) => <String, dynamic>{
      'fact': instance.fact,
    };
