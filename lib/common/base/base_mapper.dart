
abstract class BaseMapper<InitialType, OutType> {

  OutType map(InitialType model);
}