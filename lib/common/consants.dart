
class Constants {

  static const String images_base_url = 'https://api.thecatapi.com/v1/';
  static const String facts_base_url = 'https://catfact.ninja/';

  static const String cat_api_key = '59696694-a82e-4359-9e24-3213bcf4fb86';


  static const String user_model_pref_key = 'user_model';
  static const String auth_provider_pref_key = 'auth_provider';
  static const int per_page = 15;
}