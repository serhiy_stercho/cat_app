
import 'package:flutter/cupertino.dart';

class EmptyListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'No items here',
        style: TextStyle(
          fontSize: 16
        )
      ),
    );
  }

}