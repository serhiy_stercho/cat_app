import 'package:cached_network_image/cached_network_image.dart';
import 'package:cat_app/common/shared_widgets/centered_progress_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class LikableListImageItem extends StatelessWidget {

  String imageUrl;
  bool isFavorite;
  Function(bool) onFavouriteClicked;
  Function onItemClicked;

  LikableListImageItem(this.imageUrl, this.isFavorite, this.onFavouriteClicked, this.onItemClicked);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onItemClicked();
      },
      child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Positioned.fill(
              child: CachedNetworkImage(
                  imageUrl: imageUrl,
                  fit: BoxFit.cover,
                  placeholder: (context, url) => CenteredProgressIndicator(),
                  errorWidget: (context, url, error) => Icon(Icons.error)
              ),
            ),
            Container(
              color: Colors.black.withOpacity(.3),
              padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    child: Icon(
                      _obtainFavoriteIcon(),
                      color: Colors.white,
                    ),
                    onTap: () {
                      onFavouriteClicked(this.isFavorite);
                    },
                  )
                ],
              ),
            )
          ],
        ),
    );
  }

  IconData _obtainFavoriteIcon() {
    return isFavorite ? Icons.favorite : Icons.favorite_border;
  }
}
