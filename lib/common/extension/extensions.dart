

import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/persistence/database/cats_database.dart';
import 'package:cat_app/data/persistence/entities/cat_data_entity.dart';

extension CatDataEntityToCatDataModelExtension on CatDataEntityData {

  CatDataModel mapToCatDataModel() {
    final dataModel = CatDataModel();
    dataModel.id = this.id;
    dataModel.imageUrl = this.imageUrl;
    dataModel.fact = this.fact;
    dataModel.isFavorite = this.isFavorite ;
    return dataModel;
  }
}

extension FavoriteDataEntityToCatDataModelExtension on FavoriteDataEntityData {

  CatDataModel mapToCatDataModel() {
    final dataModel = CatDataModel();
    dataModel.id = this.id;
    dataModel.imageUrl = this.imageUrl;
    dataModel.fact = this.fact;
    dataModel.isFavorite = this.isFavorite ;
    return dataModel;
  }
}

extension CatDataModelToFavoriteDataEntityDataExtension on CatDataModel {

  FavoriteDataEntityData mapToFaforiteDataEntity() {
    return FavoriteDataEntityData(id: this.id, fact: this.fact, imageUrl: this.imageUrl, isFavorite: this.isFavorite);
  }
}

extension CatDataModelToCatDataEntityDataExtension on CatDataModel {

  CatDataEntityData mapToCatDataEntity() {
    return CatDataEntityData(id: this.id, fact: this.fact, imageUrl: this.imageUrl, isFavorite: this.isFavorite);
  }
}