

import 'package:cat_app/data/repository/user/user_repository.dart';
import 'package:cat_app/domain/blocs/app/app_event.dart';
import 'package:cat_app/domain/blocs/app/app_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

@injectable
class AppBloc extends Bloc<AppEvent, AppState> {

  UserRepository _userRepository;

  AppBloc(this._userRepository) : super(InitialState()) {
    add(CheckIsUserLoggedInEvent());
  }

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    switch(event.runtimeType) {
      case CheckIsUserLoggedInEvent:
        if(_userRepository.checkUserLoggedIn())
          yield UserLoggedInState();
        else yield UserUnauthorizedState();
        break;
    }
  }
}