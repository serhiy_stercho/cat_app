

import 'package:equatable/equatable.dart';

class AppState extends Equatable {

  @override
  List<Object> get props => [];
}

class InitialState extends AppState {

}

class UserUnauthorizedState extends AppState {

}

class UserLoggedInState extends AppState {

}