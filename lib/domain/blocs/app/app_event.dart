
import 'package:equatable/equatable.dart';

class AppEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class CheckIsUserLoggedInEvent extends AppEvent {
}