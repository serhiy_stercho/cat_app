

import 'package:equatable/equatable.dart';

class ProfileEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class LogoutEvent extends ProfileEvent {

}

class LoadProfileEvent extends ProfileEvent {

}