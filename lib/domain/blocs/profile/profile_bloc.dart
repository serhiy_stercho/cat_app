

import 'package:cat_app/data/model/user/user_model.dart';
import 'package:cat_app/data/repository/logout/logout_repository.dart';
import 'package:cat_app/data/repository/user/user_repository.dart';
import 'package:cat_app/domain/blocs/profile/profile_event.dart';
import 'package:cat_app/domain/blocs/profile/profile_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {

  UserRepository _userRepository;
  LogoutRepository _logoutRepository;

  ProfileBloc(this._userRepository, this._logoutRepository) : super(LoadingState());

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
      switch(event.runtimeType) {
        case LogoutEvent:
          try {
            await _logoutRepository.logout();
            yield LoggedOutState();
          } catch(exception) {
            yield ErrorState();
          }
          break;
        case LoadProfileEvent:
          UserModel model = _userRepository.getCurrentUser();
          yield ProfileLoadedState(model);
          break;
      }
  }

}