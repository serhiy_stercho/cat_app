

import 'package:cat_app/data/model/user/user_model.dart';
import 'package:equatable/equatable.dart';

class ProfileState extends Equatable {
  @override
  List<Object> get props => [];
}

class LoggedOutState extends ProfileState {}

class LoadingState extends ProfileState {}

class ProfileLoadedState extends ProfileState {

  UserModel userModel;

  ProfileLoadedState(this.userModel);

  @override
  List<Object> get props => [userModel];
}

class ErrorState extends ProfileState {

   String errMessage;

   @override
   List<Object> get props => [errMessage];
}