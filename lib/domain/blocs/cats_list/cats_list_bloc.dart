import 'package:cat_app/common/consants.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/repository/cats_data/cats_data_repository.dart';
import 'package:cat_app/data/repository/cats_data/local_cats_data_repository.dart';
import 'package:cat_app/domain/blocs/cats_list/cats_list_event.dart';
import 'package:cat_app/domain/blocs/cats_list/cats_list_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class CatsListBloc extends Bloc<CatsListEvent, CatsListState> {

  CatsDataRepository _catsDataRepository;
  LocalCatsDataRepository _localCatsDataRepository;

  int _currentPage = 0;

  List<CatDataModel> _currentDataList = [];

  CatsListBloc(this._catsDataRepository, this._localCatsDataRepository) : super(InitialState());

  @override
  Stream<CatsListState> mapEventToState(CatsListEvent event) async* {
    yield LoadingState();
    switch (event.runtimeType) {
      case GetCatDataListEvent:
        try {
          final catsDataList =
              await _catsDataRepository.getCatsData(_currentPage);
          _currentDataList.addAll(catsDataList);
          if(catsDataList.length < Constants.per_page)
            yield ListEndedState();
          yield CatListDataLoadedState(_currentDataList);
          _currentPage++;
        } catch (exception) {
          print(exception.toString());
        }
        break;
      case GetLocalCatListEvent:
        try {
          final catsDataList = await _localCatsDataRepository.fetchAll();
          yield CatListDataLoadedState(catsDataList);
        } catch (exception) {
          print(exception.toString());
        }
        break;
      case SaveAsFavourite:
        try {
            final imageId = (event as SaveAsFavourite).id;
            await _catsDataRepository.saveAsFavourite(imageId);
            _currentDataList.where((element) => element.id == imageId)
                .first.isFavorite = true;
            yield CatListDataLoadedState(_currentDataList);
        } catch(exception) {
           yield ErrorState(exception.toString());
        }
        break;
      case DeleteFromFavourites:
        try {
          final imageId = (event as DeleteFromFavourites).id;
          await _catsDataRepository.deleteFromFavourites(imageId);
          _currentDataList.where((element) => element.id == imageId)
              .first.isFavorite = false;
          yield CatListDataLoadedState(_currentDataList);
        } catch(exception) {
          yield ErrorState(exception.toString());
        }
        break;
    }
  }
}
