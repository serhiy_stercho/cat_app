import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/model/cat/cat_image.dart';
import 'package:equatable/equatable.dart';

class CatsListState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends CatsListState {}

class LoadingState extends CatsListState {}

class ListEndedState extends CatsListState {}

class CatListLoadedState extends CatsListState {

  List<CatImage> catImages;

  CatListLoadedState(this.catImages);

  @override
  List<Object> get props => [catImages];
}

class CatListDataLoadedState extends CatsListState {

  List<CatDataModel> catDataList;

  CatListDataLoadedState(this.catDataList);

  @override
  List<Object> get props => [catDataList];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is CatListDataLoadedState &&
          runtimeType == other.runtimeType &&
          catDataList == other.catDataList;

  @override
  int get hashCode => super.hashCode ^ catDataList.hashCode;
}

class ErrorState extends CatsListState {

  String errMessage;

  ErrorState(this.errMessage);

  @override
  List<Object> get props => [errMessage];
}
