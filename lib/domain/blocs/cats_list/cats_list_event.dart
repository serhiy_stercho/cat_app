
import 'package:equatable/equatable.dart';

class CatsListEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class GetCatDataListEvent extends CatsListEvent {

  GetCatDataListEvent();

  @override
  List<Object> get props => [];
}

class GetLocalCatListEvent extends CatsListEvent {

  GetLocalCatListEvent();

  @override
  List<Object> get props => [];
}


class SaveAsFavourite extends CatsListEvent {

  String id;

  SaveAsFavourite(this.id);

  @override
  List<Object> get props => [id];
}

class DeleteFromFavourites extends CatsListEvent {

  String id;

  DeleteFromFavourites(this.id);

  @override
  List<Object> get props => [id];
}