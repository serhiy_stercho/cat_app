

import 'package:equatable/equatable.dart';

class FavoritesListEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class GetFavouritesEvent extends FavoritesListEvent {

}

class GetSavedLocallyFavouritesEvent extends FavoritesListEvent {

}