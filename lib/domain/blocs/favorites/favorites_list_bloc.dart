

import 'package:cat_app/common/consants.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/data/repository/cats_data/cats_data_repository.dart';
import 'package:cat_app/data/repository/cats_data/local_cats_data_repository.dart';
import 'package:cat_app/domain/blocs/favorites/favorites_list_event.dart';
import 'package:cat_app/domain/blocs/favorites/favorites_list_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';


@injectable
class FavoritesListBloc extends Bloc<FavoritesListEvent, FavoritesListState> {

  CatsDataRepository _catsDataRepository;
  LocalCatsDataRepository _localCatsDataRepository;
  int _currentPage = 0;

  List<CatDataModel> _currentDataList = [];

  FavoritesListBloc(this._catsDataRepository, this._localCatsDataRepository) : super(InitialState());

  @override
  Stream<FavoritesListState> mapEventToState(FavoritesListEvent event) async* {
      yield LoadingState();
      if(event is GetFavouritesEvent) {
        try {
          final favouritesList = await _catsDataRepository
              .getFavouritesData(_currentPage);
          if(favouritesList.length < Constants.per_page)
            yield ListEndedState();
          _currentDataList.addAll(favouritesList);
          yield FavoritesListLoadedState(_currentDataList);
          _currentPage++;
        } catch(exception) {
          yield ErrorState(exception.toString());
        }
      } else if(event is GetSavedLocallyFavouritesEvent) {
        try {
          final favorites = await _localCatsDataRepository.selectAllFavorites();
          yield FavoritesListLoadedState(favorites);
        }catch(e) {
          yield ErrorState(e.toString());
        }

      }
  }

}