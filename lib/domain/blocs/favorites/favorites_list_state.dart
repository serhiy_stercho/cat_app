

import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:equatable/equatable.dart';

class FavoritesListState extends Equatable {

  @override
  List<Object> get props => [];

}

class LoadingState extends FavoritesListState {

}

class InitialState extends FavoritesListState {

}

class ListEndedState extends FavoritesListState {

}

class FavoritesListLoadedState extends FavoritesListState {

  List<CatDataModel> catDataList;

  FavoritesListLoadedState(this.catDataList);

  @override
  List<Object> get props => [catDataList];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is FavoritesListLoadedState &&
              runtimeType == other.runtimeType &&
              catDataList == other.catDataList;

  @override
  int get hashCode => super.hashCode ^ catDataList.hashCode;
}

class ErrorState extends FavoritesListState {

  String errMessage;

  ErrorState(this.errMessage);

  @override
  List<Object> get props => [errMessage];
}