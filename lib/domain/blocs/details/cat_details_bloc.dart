
import 'package:cat_app/data/repository/cats_data/cats_data_repository.dart';
import 'package:cat_app/domain/blocs/details/cat_delails_state.dart';
import 'package:cat_app/domain/blocs/details/cat_details_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class CatDetailsBloc extends Bloc<CatDetailsEvent, CatDetailsState> {

  CatsDataRepository _catsDataRepository;

  CatDetailsBloc(this._catsDataRepository) : super(InitialState());

  @override
  Stream<CatDetailsState> mapEventToState(CatDetailsEvent event) async* {
      yield LoadingState();
      if(event is SaveAsFavorite) {
        try {
          await _catsDataRepository.saveAsFavourite(event.id);
          yield SavedAsFavorite();
        } catch(exception) {

        }
      }
  }
}