
import 'package:equatable/equatable.dart';

class CatDetailsState extends Equatable {

  @override
  List<Object> get props => [];
}

class InitialState extends CatDetailsState {}

class LoadingState extends CatDetailsState {}

class SavedAsFavorite extends CatDetailsState {}