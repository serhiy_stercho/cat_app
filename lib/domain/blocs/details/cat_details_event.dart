
import 'package:equatable/equatable.dart';

class CatDetailsEvent extends Equatable {

  @override
  List<Object> get props => [];
}

class SaveAsFavorite extends CatDetailsEvent {

  String id;

  SaveAsFavorite(this.id);

  @override
  List<Object> get props => [id];
}