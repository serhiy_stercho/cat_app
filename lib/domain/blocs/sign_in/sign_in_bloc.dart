import 'package:cat_app/data/repository/sign_in/facebook_sign_in_repository.dart';
import 'package:cat_app/data/repository/sign_in/google_sign_in_repository.dart';
import 'package:cat_app/data/repository/sign_in/sign_in_reposirory.dart';
import 'package:cat_app/domain/blocs/sign_in/sign_in_event.dart';
import 'package:cat_app/domain/blocs/sign_in/sign_in_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final SignInRepository _googleSignInRepository;
  final SignInRepository _facebookSignInRepository;

  SignInBloc(@Named.from(GoogleSignInRepository) this._googleSignInRepository,
      @Named.from(FacebookSignInRepository) this._facebookSignInRepository)
      : super(InitialState());

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    yield LoadingState();
    try {
      switch (event.runtimeType) {
        case SignInWithGoogleEvent:
          {
            try {
              await _googleSignInRepository.performSignIn();
              yield SuccessfullySignedInState();
            } catch(exception) {

            }
          }
          break;
        case SignInWithFacebookEvent:
          try {
            await _facebookSignInRepository.performSignIn();
            yield SuccessfullySignedInState();
          } catch(ex) {

          }
          break;
      }
    } catch (_) {
      yield SignInErrorState();
    }
  }
}
