

import 'package:equatable/equatable.dart';

class SignInState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialState extends SignInState {

}

class SuccessfullySignedInState extends SignInState {

}

class SignInErrorState extends SignInState {

}

class LoadingState extends SignInState {

}