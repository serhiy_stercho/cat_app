

import 'package:equatable/equatable.dart';

class SignInEvent extends Equatable {

  @override
  List<Object> get props => [];

}

class InitialEvent extends SignInEvent {

}

class SignInWithGoogleEvent extends SignInEvent {
}

class SignInWithFacebookEvent extends SignInEvent {
}