

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cat_app/common/shared_widgets/centered_progress_indicator.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/details/cat_delails_state.dart';
import 'package:cat_app/domain/blocs/details/cat_details_bloc.dart';
import 'package:cat_app/domain/blocs/details/cat_details_event.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CatDetailsPage extends StatefulWidget {

  CatDataModel _catDataModel;

  CatDetailsPage(this._catDataModel);

  @override
  State<StatefulWidget> createState() {
    return _CatDetailState(_catDataModel);
  }

}

class _CatDetailState extends State<CatDetailsPage> {

  CatDataModel _catDataModel;
  CatDetailsBloc _catDetailsBloc;

  _CatDetailState(this._catDataModel);

  @override
  void initState() {
    super.initState();
    _catDetailsBloc = getIt.get<CatDetailsBloc>();
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
          appBar: AppBar(
            title: Text('Detail'),
            actions: [
              InkWell(
                child: Icon(
                  _obtainFavoriteIcon(_catDataModel.isFavorite),
                  color: Colors.white,
                ),
                onTap: () {
                  if(!_catDataModel.isFavorite) 
                    _catDetailsBloc.add(SaveAsFavorite(_catDataModel.id));
                },
              )
            ],
          ),
          body: SafeArea(
            child: BlocListener(
              listener: (context, state) {
                if(state is SavedAsFavorite) {
                  setState(() {
                    _catDataModel.isFavorite = true;
                  });
                }
              },
              cubit: _catDetailsBloc,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    height: 260,
                    child: Positioned.fill(
                      child: CachedNetworkImage(
                          imageUrl: _catDataModel.imageUrl,
                          fit: BoxFit.cover,
                          placeholder: (context, url) => CenteredProgressIndicator(),
                          errorWidget: (context, url, error) => Icon(Icons.error)
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: Text(
                        _catDataModel.fact,
                      style: TextStyle(
                        letterSpacing: 1,
                        fontSize: 18,
                        color: Colors.black
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
      );
  }

  IconData _obtainFavoriteIcon(bool favorite) {
    return favorite ? Icons.favorite : Icons.favorite_border;
  }

}