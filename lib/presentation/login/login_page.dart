import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/sign_in/sign_in_bloc.dart';
import 'package:cat_app/domain/blocs/sign_in/sign_in_event.dart';
import 'package:cat_app/domain/blocs/sign_in/sign_in_state.dart';
import 'package:cat_app/presentation/main/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  SignInBloc _signInBlock;

  @override
  void initState() {
    super.initState();
    _signInBlock = getIt.get<SignInBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: BlocListener(
            cubit: _signInBlock,
            listener: (context, state) {
              _renderState(state);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  child: Container(
                      height: 42,
                      margin: EdgeInsets.symmetric(vertical: 12),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(18),
                          color: Colors.white,
                          boxShadow: [BoxShadow(color: Colors.grey.shade500)]),
                      child: Row(
                        children: [
                          SizedBox(width: 24),
                          Icon(FontAwesome5Brands.google),
                          SizedBox(width: 24),
                          Text(
                            'Login with Google',
                            style: TextStyle(fontSize: 18, color: Colors.black),
                          ),
                        ],
                      )),
                  onTap: () {
                    _signInBlock.add(SignInWithGoogleEvent());
                  },
                ),
                InkWell(
                  child: Container(
                    height: 42,
                    margin: EdgeInsets.fromLTRB(0, 12, 0, 36),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        color: Colors.blueAccent,
                        boxShadow: [BoxShadow(color: Colors.grey.shade500)]),
                    child: Row(
                      children: [
                        SizedBox(width: 24),
                        Icon(FontAwesome.facebook, color: Colors.white),
                        SizedBox(width: 24),
                        Text(
                          'Login with Facebook',
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  onTap: () {
                    _signInBlock.add(SignInWithFacebookEvent());
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }


  void _renderState(SignInState state) {
    switch (state.runtimeType) {
      case SuccessfullySignedInState:
        dismissProgressDialog();
        _navigateToMainPage();
        break;
      case SignInErrorState:
        dismissProgressDialog();
        break;
      case LoadingState:
        showProgressDialog(context: context, loadingText: 'Loading..');
        break;
    }
  }

  void _navigateToMainPage() {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => MainPage()));
  }
}
