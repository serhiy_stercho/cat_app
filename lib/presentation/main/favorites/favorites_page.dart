

import 'dart:async';

import 'package:cat_app/common/shared_widgets/likable_list_image_item.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/favorites/favorites_list_bloc.dart';
import 'package:cat_app/domain/blocs/favorites/favorites_list_event.dart';
import 'package:cat_app/domain/blocs/favorites/favorites_list_state.dart';
import 'package:cat_app/presentation/details/cat_details_page.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';

class FavoritesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
      return _FavoritePageState();
  }
}

class _FavoritePageState extends State<FavoritesPage> {

  FavoritesListBloc _favoritesListBloc;

  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _favoritesListBloc = getIt.get<FavoritesListBloc>();
    _checkConnectivity();
  }

  void _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if(connectivityResult == ConnectivityResult.none) {
      removeScrollListener();
      _favoritesListBloc.add(GetSavedLocallyFavouritesEvent());
    } else {
      _scrollController.addListener(_onScroll);
      _favoritesListBloc.add(GetFavouritesEvent());
    }
  }


  @override
  void dispose() {
    _scrollController.dispose();
    _favoritesListBloc.close();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll == 0 &&
        !(_favoritesListBloc.state is LoadingState)) {
      _favoritesListBloc.add(GetFavouritesEvent());
    }
  }

  void removeScrollListener() {
    _scrollController.removeListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
            padding: EdgeInsets.all(12),
            child: BlocConsumer<FavoritesListBloc, FavoritesListState>(
              cubit: _favoritesListBloc,
              listener: (context, state) {
                if (state is LoadingState) {
                  showProgressDialog(context: context, loadingText: 'Loading..');
                } else if (state is FavoritesListLoadedState) {
                  dismissProgressDialog();
                } else if(state is ListEndedState) {
                  removeScrollListener();
                }
              },
              buildWhen: (previous, current) {
                return current is FavoritesListLoadedState;
              },
              builder: (context, state) {
                if (state is FavoritesListLoadedState) {
                  return GridView.builder(
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: 200,
                          childAspectRatio: 4 / 3,
                          crossAxisSpacing: 15,
                          mainAxisSpacing: 15),
                      itemCount: state.catDataList.length,
                      controller: _scrollController,
                      itemBuilder: (context, index) {
                        return LikableListImageItem(
                            state.catDataList[index].imageUrl,
                            state.catDataList[index].isFavorite,
                                (favourite) {
                              // if(favourite)
                              //   _catsListBloc.add(DeleteFromFavourites(state.catDataList[index].id));
                              // else _catsListBloc.add(SaveAsFavourite(state.catDataList[index].id));
                            },
                            () { _navigateToDetail(state.catDataList[index]); }
                            );
                      });
                } else {
                  return Container();
                }
              },
            )),
      ),
    );
  }

  void _navigateToDetail(CatDataModel model) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CatDetailsPage(model)));
  }
}