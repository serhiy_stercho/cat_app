import 'dart:async';

import 'package:cat_app/common/shared_widgets/centered_progress_indicator.dart';
import 'package:cat_app/common/shared_widgets/likable_list_image_item.dart';
import 'package:cat_app/data/model/cat/cat_data_model.dart';
import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/cats_list/cats_list_bloc.dart';
import 'package:cat_app/domain/blocs/cats_list/cats_list_event.dart';
import 'package:cat_app/domain/blocs/cats_list/cats_list_state.dart';
import 'package:cat_app/presentation/details/cat_details_page.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_progress_dialog/flutter_progress_dialog.dart';

class CatsListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CatsListPageState();
  }
}

class _CatsListPageState extends State<CatsListPage> {

  CatsListBloc _catsListBloc;

  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _catsListBloc = getIt.get<CatsListBloc>();
    _checkConnectivity();

  }

  void _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if(connectivityResult == ConnectivityResult.none) {
      removeScrollListener();
      _catsListBloc.add(GetLocalCatListEvent());
    } else {
      _scrollController.addListener(_onScroll);
      _catsListBloc.add(GetCatDataListEvent());
    }
  }

  void removeScrollListener() {
    _scrollController.removeListener(_onScroll);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Padding(
          padding: EdgeInsets.all(12),
          child: BlocConsumer<CatsListBloc, CatsListState>(
            cubit: _catsListBloc,
            listener: (context, state) {
              if (state is LoadingState) {
                showProgressDialog(context: context, loadingText: 'Loading..');
              } else if (state is CatListDataLoadedState) {
                dismissProgressDialog();
              } else if(state is CatListLoadedState) {
                removeScrollListener();
              }
            },
            buildWhen: (previous, current) {
              return current is CatListDataLoadedState;
            },
            builder: (context, state) {
              if (state is CatListDataLoadedState) {
                return GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        childAspectRatio: 4 / 3,
                        crossAxisSpacing: 15,
                        mainAxisSpacing: 15),
                    itemCount: state.catDataList.length,
                    controller: _scrollController,
                    itemBuilder: (context, index) {
                      return LikableListImageItem(
                          state.catDataList[index].imageUrl,
                          state.catDataList[index].isFavorite,
                          (favourite) {
                            if(favourite)
                              _catsListBloc.add(DeleteFromFavourites(state.catDataList[index].id));
                            else _catsListBloc.add(SaveAsFavourite(state.catDataList[index].id));
                          },
                          () { _navigateToDetail(state.catDataList[index]); }
                          );
                    });
              } else {
                return Container();
              }
            },
          )),
    ));
  }

  void _navigateToDetail(CatDataModel model) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => CatDetailsPage(model)));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _catsListBloc.close();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll == 0 &&
        !(_catsListBloc.state is LoadingState)) {
      _catsListBloc.add(GetCatDataListEvent());
    }
  }
}
