import 'package:cat_app/data/model/user/user_model.dart';
import 'package:cat_app/data/persistence/database/cats_database.dart';
import 'package:cat_app/di/di_initializer.dart';
import 'package:cat_app/domain/blocs/profile/profile_bloc.dart';
import 'package:cat_app/domain/blocs/profile/profile_event.dart';
import 'package:cat_app/domain/blocs/profile/profile_state.dart';
import 'package:cat_app/presentation/login/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:moor_db_viewer/moor_db_viewer.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<ProfilePage> {
  ProfileBloc _profileBloc;

  @override
  void initState() {
    super.initState();
    _profileBloc = getIt.get<ProfileBloc>();
    _profileBloc.add(LoadProfileEvent());
  }

  void _navigateToLoginPage() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => LoginPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(24),
          child: BlocConsumer<ProfileBloc, ProfileState>(
            cubit: _profileBloc,
            listener: (context, state) {
              if(state is LoggedOutState) {
                _navigateToLoginPage();
              }
            },
            buildWhen: (previous, current) {
              return !(current is LoggedOutState);
            },
            builder: (context, state) {
              return buildWidgetDependingOnState(state);
            },
          ),
        ),
      ),
    );
  }

  Widget buildWidgetDependingOnState(ProfileState state) {
    switch (state.runtimeType) {
      case ProfileLoadedState:
        return _buildUserProfileWidget((state as ProfileLoadedState).userModel);
        break;
      case LoadingState:
        return _buildLoadingWidget();
        break;
    }
  }

  Widget _buildUserProfileWidget(UserModel userModel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        CircleAvatar(
            radius: 82, backgroundImage: NetworkImage(userModel.avatarUrl)),
        SizedBox(height: 36),
        Divider(height: 1, color: Colors.grey.shade400),
        SizedBox(height: 24),
        Text('Full name:',
          style: _titleTextStyle(),
        ),
        SizedBox(height: 4),
        Text(userModel.displayName,
          style: _subtitleTextStyle(),
        ),
        SizedBox(height: 16),
        Text('Email:',
          style: _titleTextStyle(),
        ),
        SizedBox(height: 4),
        Text(userModel.email,
          style: _subtitleTextStyle(),
        ),
        SizedBox(height: 16),
        InkWell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text('Log out',
              style: _logoutBtnTextStyle(),
            ),
          ),
          onTap: () => {
            _profileBloc.add(LogoutEvent())
          },
        ),
        SizedBox(height: 16),
        InkWell(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text('Open db viewer',
              style: _logoutBtnTextStyle(),
            ),
          ),
          onTap: () => {
             Navigator.of(context).push(MaterialPageRoute(builder: (context) => MoorDbViewer(getIt.get<CatsDatabase>())))
          },
        )
      ],
    );
  }

  Widget _buildLoadingWidget() {
    return Center(child: CircularProgressIndicator());
  }

  TextStyle _titleTextStyle() {
    return TextStyle(
      color: Colors.black,
      fontSize: 18
    );
  }

  TextStyle _subtitleTextStyle() {
    return TextStyle(
        color: Colors.grey.shade400,
        fontSize: 18
    );
  }

  TextStyle _logoutBtnTextStyle() {
    return TextStyle(
        color: Colors.teal.shade500,
        fontSize: 21
    );
  }
}
