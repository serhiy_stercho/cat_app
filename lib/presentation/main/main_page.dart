import 'package:cat_app/presentation/main/favorites/favorites_page.dart';
import 'package:cat_app/presentation/main/list/cats_list_page.dart';
import 'package:cat_app/presentation/main/profile/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:network_logger/network_logger.dart';

class MainPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {

  int _selectedPageIndex = 0;


  @override
  void initState() {
    NetworkLoggerOverlay.attachTo(context);
    super.initState();
  }

  static List<Widget> _pagesList = <Widget>[
    CatsListPage(),
    FavoritesPage(),
    ProfilePage()
  ];

  static  List<BottomNavigationBarItem> _navItems = <BottomNavigationBarItem>[
    BottomNavigationBarItem(
        icon: Icon(CupertinoIcons.square_grid_2x2),
        label: 'Cats'
    ),
    BottomNavigationBarItem(
        icon: Icon(CupertinoIcons.heart),
        label: 'Favorites'
    ),
    BottomNavigationBarItem(
        icon: Icon(CupertinoIcons.person),
        label: 'Profile'
    ),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pagesList.elementAt(_selectedPageIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: _navItems,
        selectedItemColor: Colors.teal.shade500,
        unselectedItemColor: Colors.black,
        currentIndex: _selectedPageIndex,
        onTap: (index) {
          setState(() {
            _selectedPageIndex = index;
          });
        },
      ),
    );
  }

}
