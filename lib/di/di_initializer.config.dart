// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i3;
import 'package:flutter_facebook_login/flutter_facebook_login.dart' as _i6;
import 'package:get_it/get_it.dart' as _i1;
import 'package:google_sign_in/google_sign_in.dart' as _i7;
import 'package:injectable/injectable.dart' as _i2;
import 'package:network_logger/network_logger.dart' as _i5;
import 'package:shared_preferences/shared_preferences.dart' as _i8;

import '../data/mapper/cat_data_to_cat_model_mapper.dart' as _i4;
import '../data/network/api_client.dart' as _i23;
import '../data/persistence/database/cats_database.dart' as _i20;
import '../data/repository/cats_data/cats_data_repository.dart' as _i16;
import '../data/repository/cats_data/local_cats_data_repository.dart' as _i17;
import '../data/repository/logout/logout_repository.dart' as _i12;
import '../data/repository/sign_in/facebook_sign_in_repository.dart' as _i22;
import '../data/repository/sign_in/google_sign_in_repository.dart' as _i21;
import '../data/repository/sign_in/sign_in_reposirory.dart' as _i14;
import '../data/repository/user/user_repository.dart' as _i10;
import '../domain/blocs/app/app_bloc.dart' as _i9;
import '../domain/blocs/cats_list/cats_list_bloc.dart' as _i15;
import '../domain/blocs/details/cat_details_bloc.dart' as _i19;
import '../domain/blocs/favorites/favorites_list_bloc.dart' as _i18;
import '../domain/blocs/profile/profile_bloc.dart' as _i11;
import '../domain/blocs/sign_in/sign_in_bloc.dart' as _i13;
import 'modules/app_module.dart' as _i26;
import 'modules/auth_module.dart' as _i25;
import 'modules/network_module.dart'
    as _i24; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String environment, _i2.EnvironmentFilter environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final networkModule = _$NetworkModule();
  final authModule = _$AuthModule();
  final appModule = _$AppModule();
  gh.factory<_i3.BaseOptions>(() => networkModule.imagesDioBaseOptions,
      instanceName: 'images_dio_client_base_options');
  gh.factory<_i3.BaseOptions>(() => networkModule.factsDioBaseOptions,
      instanceName: 'facts_dio_client_base_options');
  gh.factory<_i4.CatDataToCatModelMapper>(() => _i4.CatDataToCatModelMapper());
  gh.factory<_i5.DioNetworkLogger>(
      () => networkModule.networkLoggerInterceptor);
  gh.factory<_i6.FacebookLogin>(() => authModule.facebookLogin);
  gh.factory<_i7.GoogleSignIn>(() => authModule.googleSignIn);
  await gh.factoryAsync<_i8.SharedPreferences>(() => appModule.prefs,
      preResolve: true);
  gh.factory<_i9.AppBloc>(() => _i9.AppBloc(get<_i10.UserRepository>()));
  gh.factory<_i3.Dio>(
      () => networkModule.provideImagesDioClient(
          get<_i3.BaseOptions>(instanceName: 'images_dio_client_base_options'),
          get<_i5.DioNetworkLogger>()),
      instanceName: 'images_dio_client');
  gh.factory<_i3.Dio>(
      () => networkModule.provideFactsDioClient(
          get<_i3.BaseOptions>(instanceName: 'facts_dio_client_base_options'),
          get<_i5.DioNetworkLogger>()),
      instanceName: 'facts_dio_client');
  gh.factory<_i11.ProfileBloc>(() => _i11.ProfileBloc(
      get<_i10.UserRepository>(), get<_i12.LogoutRepository>()));
  gh.factory<_i13.SignInBloc>(() => _i13.SignInBloc(
      get<_i14.SignInRepository>(instanceName: 'GoogleSignInRepository'),
      get<_i14.SignInRepository>(instanceName: 'FacebookSignInRepository')));
  gh.factory<_i15.CatsListBloc>(() => _i15.CatsListBloc(
      get<_i16.CatsDataRepository>(), get<_i17.LocalCatsDataRepository>()));
  gh.factory<_i18.FavoritesListBloc>(() => _i18.FavoritesListBloc(
      get<_i16.CatsDataRepository>(), get<_i17.LocalCatsDataRepository>()));
  gh.factory<_i19.CatDetailsBloc>(
      () => _i19.CatDetailsBloc(get<_i16.CatsDataRepository>()));
  gh.singleton<_i20.CatsDatabase>(_i20.CatsDatabase());
  gh.singleton<_i17.LocalCatsDataRepository>(
      _i17.LocalCatsDataRepository(get<_i20.CatsDatabase>()));
  gh.singleton<_i14.SignInRepository>(
      _i21.GoogleSignInRepository(
          get<_i7.GoogleSignIn>(), get<_i8.SharedPreferences>()),
      instanceName: 'GoogleSignInRepository');
  gh.singleton<_i14.SignInRepository>(
      _i22.FacebookSignInRepository(
          get<_i6.FacebookLogin>(), get<_i8.SharedPreferences>()),
      instanceName: 'FacebookSignInRepository');
  gh.singleton<_i10.UserRepository>(
      _i10.UserRepository(get<_i8.SharedPreferences>()));
  gh.singleton<_i12.LogoutRepository>(_i12.LogoutRepository(
      get<_i10.UserRepository>(),
      get<_i7.GoogleSignIn>(),
      get<_i6.FacebookLogin>()));
  gh.singleton<_i23.ApiClient>(_i23.ApiClient(
      get<_i3.Dio>(instanceName: 'images_dio_client'),
      get<_i3.Dio>(instanceName: 'facts_dio_client')));
  gh.singleton<_i16.CatsDataRepository>(_i16.CatsDataRepository(
      get<_i23.ApiClient>(),
      get<_i4.CatDataToCatModelMapper>(),
      get<_i10.UserRepository>(),
      get<_i17.LocalCatsDataRepository>()));
  return get;
}

class _$NetworkModule extends _i24.NetworkModule {}

class _$AuthModule extends _i25.AuthModule {}

class _$AppModule extends _i26.AppModule {}
