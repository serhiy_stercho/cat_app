import 'package:cat_app/common/consants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:network_logger/network_logger.dart';

@module
abstract class NetworkModule {
  @lazySingleton
  DioNetworkLogger get networkLoggerInterceptor => DioNetworkLogger();

  @Named('images_dio_client_base_options')
  BaseOptions get imagesDioBaseOptions => BaseOptions(
      baseUrl: Constants.images_base_url,
      receiveTimeout: 25000,
      connectTimeout: 25000,
      headers: {'x-api-key': Constants.cat_api_key});

  @Named('facts_dio_client_base_options')
  BaseOptions get factsDioBaseOptions => BaseOptions(
      baseUrl: Constants.facts_base_url,
      receiveTimeout: 25000,
      connectTimeout: 25000);

  @lazySingleton
  @Named('images_dio_client')
  Dio provideImagesDioClient(
      @Named('images_dio_client_base_options') BaseOptions dioOptions,
      DioNetworkLogger _networkLoggerInterceptor) {
    return Dio(dioOptions)..interceptors.add(_networkLoggerInterceptor);
  }

  @lazySingleton
  @Named('facts_dio_client')
  Dio provideFactsDioClient(
      @Named('facts_dio_client_base_options') BaseOptions dioOptions,
      DioNetworkLogger _networkLoggerInterceptor) {
    return Dio(dioOptions)..interceptors.add(_networkLoggerInterceptor);
  }
}
