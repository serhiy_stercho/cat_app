
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

@module
abstract class AuthModule {

  @lazySingleton
  GoogleSignIn get googleSignIn => GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ]
  );

  @lazySingleton
  FacebookLogin get facebookLogin => FacebookLogin();
}